<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('offer.html', function () {
    return view('offer');
});
Route::get('offer2.html', function () {
    return view('offer2');
});
Route::get('index.html', function () {
    return view('index');
});
