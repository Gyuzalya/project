<!DOCTYPE html>
<html lang="en">
<head>
  <title>Smartphone.kz</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
<body>

  <!-- Image and text -->

  <nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
    <a href="#" class="navbar-brand"> <img src="img/s1.png" width="30" height="30" alt="logo"> </a>
  <!-- Brand -->
  <a class="navbar-brand" href="index.html">Главная</a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#"> <i class="fas fa-map-marker-alt"></i> Магазины</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-shopping-cart"></i> Доставка</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"> <i class="fas fa-truck"></i> Оптовая покупка</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"> <i class="fas fa-mobile-alt"></i> Контакты</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input type="text" class="form-control mr-sm-2" placeholder="Поиск" aria-label="Поиск">
<button class="btn btn-outline-success my-2 my-sm-2"> <i class="fab fa-sistrix"></i> Поиск</button>
</form>
<form class="ml-auto">
<button class=" btn btn-outline-warning">Регистрация</button>
<button class="btn btn-outline-info">Вход</button>
</form>
  </div>
</nav>

<div class="container-fluid">
<div class="row">
  <div class="col-md-3 bg-light"  >


  <div class="dropdown ml-5">
  <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-mobile-alt"></i>
    Выберите смартфон
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
    <button class="dropdown-item" type="button">Apple</button>
    <button class="dropdown-item" type="button">OnePlus</button>
    <button class="dropdown-item" type="button">Xiaomi </button>
    <button class="dropdown-item" type="button">Samsung</button>
    <button class="dropdown-item" type="button">Meizu</button>
    <button class="dropdown-item" type="button">LG </button>
    <button class="dropdown-item" type="button">Oppo</button>
    <button class="dropdown-item" type="button">Xperia</button>
    <button class="dropdown-item" type="button">Huawei</button>
  </div>
 </div>
</div>
  <div class="col-md-6 bg-light " >

  <button type="button" class="btn btn-light"> <i class="fas fa-check"></i> Защита покупателей</button>
  <button type="button" class="btn btn-light"> <i class="far fa-lightbulb"></i> Нужна помощь</button>
  <button type="button" class="btn btn-light"> <i class="far fa-envelope-open"></i> Обратная связь</button>

</div>
  <div class="col-md-3 bg-light "  >
    <div class="dropdown ml-5">
  <button class="btn btn-light dropdown-toggle"  type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="fas fa-building"></i> Выберите город
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
    <button class="dropdown-item" type="button">Алматы</button>
    <button class="dropdown-item" type="button">Астана</button>
    <button class="dropdown-item" type="button">Актау</button>
    <button class="dropdown-item" type="button">Актобе</button>
    <button class="dropdown-item" type="button">Атырау</button>
    <button class="dropdown-item" type="button">Кызылорда</button>
    <button class="dropdown-item" type="button">Талдыкорган</button>
    <button class="dropdown-item" type="button">Кокшетау</button>
    <button class="dropdown-item" type="button">Шымкент</button>
  </div>
</div>
</div>
</div>
</div>

<div class="row bg-light">
<div class="mx-auto"> <h2><br>Xiaomi Redmi Note 5A Prime 32Gb  </h2>
 </div>
 </div>

<div class="container-fluid bg-light">

  <div class="row">
<div class="col-md-6">
  <div id="demo" class="carousel slide" data-ride="carousel">

<!-- Indicators -->
<ul class="carousel-indicators">
  <li data-target="#demo" data-slide-to="0" class="active"></li>
  <li data-target="#demo" data-slide-to="1"></li>
  <li data-target="#demo" data-slide-to="2"></li>
</ul>

<!-- The slideshow -->
<div class="carousel-inner m">
  <div class="carousel-item active">
    <img src="img/1sll.jpg" width="100%" height="400" alt="First slide">
  </div>
  <div class="carousel-item">
    <img src="img/2sl.jpg" width="100%" height="400" alt="Second slide">
  </div>
  <div class="carousel-item">
    <img src="img/3sl.jpg" width="100%" height="400" alt="Third slide">
  </div>
</div>

<!-- Left and right controls -->
<a class="carousel-control-prev" href="#demo" data-slide="prev">
  <span class="carousel-control-prev-icon"></span>
</a>
<a class="carousel-control-next" href="#demo" data-slide="next">
  <span class="carousel-control-next-icon"></span>
</a>

</div>
</div>

<div class="col-md-3">
Характеристики:<br>
Тип: моноблок<br>
Цвет: серый,черный<br>
Оперативная память: 3072мб<br>
Объем внутренней памяти: 32гб<br>
ОС: Android7.1<br>
Диагональ:5,5<br>
Разрешение дисплея:720х1280<br>
Процессор:8яд.Snapdragon435<br>
Фотокамера:13мпикс<br>

</div>

<div class="col-md-3">
<strike>Цена 69000тнг</strike> <br> Цена 59000тнг<br>
<i class="fas fa-check"></i>  Бесплатная доставка<br>
<i class="fas fa-check"></i>  Одобрание кредита Онлайн Kaspi.kz<br>
<i class="fas fa-check"></i>  Высокий уровень одобрения<br>
<i class="fas fa-check"></i>  Возврат товара в течение 14 дней<br><br>
<button type="button" class="btn btn-info" style="font-size:40px"  > <i class="fas fa-flag-checkered"></i>   Заказать</button>


</div>

  </div>
</div>

</div>
<div class="container-fluid bg-light">
<div class="row ">
<div class="mx-auto"> <h2><br>Похожие  смартфоны </h2>
 </div>
 </div>

<div class="row " >
  <div class="col-md-3">
    <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="img/1ost.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Huawei M3s</h5>
    <p class="card-text">Цена 69000тнг</p>
    <a href="#" class="btn btn-primary">Подробнее</a>
  </div>
</div>
  </div>

  <div class="col-md-3">
    <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="img/2ostjpg.jpg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Iphone6s 64Gb</h5>
    <p class="card-text">Цена 149000тнг</p>
    <a href="#" class="btn btn-primary">Подробнее</a>
  </div>
  </div>
  </div>

  <div class="col-md-3">
    <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="img/3ost.jpg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Huawei P10 Lite</h5>
    <p class="card-text">Цена 79000тнг</p>
    <a href="#" class="btn btn-primary">Подробнее</a>
  </div>
  </div>
  </div>

  <div class="col-md-3">
    <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="img/4ost.jpg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Huawei P8 Lite</h5>
    <p class="card-text">Цена 160000тнг</p>
    <a href="#" class="btn btn-primary">Подробнее</a>
  </div>
</div>
  </div>

</div>
<br><br>
<div class="row">
  <div class="col-md-3">
    <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="img/5ost.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">OPPO 8l</h5>
    <p class="card-text">Цена 59000тнг</p>
    <a href="#" class="btn btn-primary">Подробнее</a>
  </div>
</div>
  </div>

  <div class="col-md-3">
    <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="img/6ost.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">OPPO 10</h5>
    <p class="card-text">Цена 79000тнг</p>
    <a href="#" class="btn btn-primary">Подробнее</a>
  </div>
  </div>
  </div>

  <div class="col-md-3">
    <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="img/7ost.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Samsung Galaxy J3</h5>
    <p class="card-text">Цена 59000тнг</p>
    <a href="#" class="btn btn-primary">Подробнее</a>
  </div>
  </div>
  </div>

  <div class="col-md-3">
    <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="img/8ost.jpg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Samsung Galaxy J3</h5>
    <p class="card-text">Цена 59000тнг</p>
    <a href="#" class="btn btn-primary">Подробнее</a>
  </div>
</div>
  </div>

</div>
</div>

<div class="container-fluid bg-dark">
<div class="row">
  <div class="mx-auto">
    <button type="button" class="btn btn-dark" style="font-size:25px"  > <i class="fab fa-vk"></i></button>
<button type="button" class="btn btn-dark" style="font-size:25px" ><i class="fab fa-instagram"></i></button>
<button type="button" class="btn btn-dark" style="font-size:25px" ><i class="fab fa-facebook-square"></i></button>
<button type="button" class="btn btn-dark" style="font-size:25px" ><i class="fab fa-whatsapp"></i></button>
<button type="button" class="btn btn-dark" style="font-size:25px" ><i class="fab fa-youtube"></i></button>

   </div>
</div>

</div>
</body>
</html>
